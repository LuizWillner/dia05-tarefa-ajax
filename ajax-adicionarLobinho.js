function isInt(value) {
    let x;
    if (isNaN(value)) {
      return false;
    }
    x = parseFloat(value);
    return (x | 0) === x;
}



const url = "http://lobinhos.herokuapp.com/";
const complement = "wolves/";

// ================ GET =================================
// fetch(url+complement)
// .then(resp => { return resp.json()})
// .then(allWolves => console.log(allWolves))
// .catch(error => console.log(error));

document.querySelector("#botaoSalvar").addEventListener("click", (e) => {
    e.preventDefault();
    let wolfName = document.querySelector("#nomeDoLobo").value;
    let wolfAge = document.querySelector("#idade").value;
    let pictureLink = document.querySelector("#linkFoto").value;
    let wolfDescription = document.querySelector("#descricao").value;

    if(wolfName != "" && wolfAge != "" && pictureLink != "" && wolfDescription != ""){
        if(isInt(wolfAge) && wolfAge > 0 && wolfAge < 100) {
            if(wolfName.length >= 4 && wolfName.length <= 60 && wolfDescription.length >= 10 && wolfDescription.length <= 255) {
                console.log(wolfName);
                console.log(wolfAge);
                console.log(pictureLink);
                console.log(wolfDescription);
                // =================== POST =======================
                let fetchBody = {
                    wolf: {
                        name: wolfName,
                        age: wolfAge,
                        link_image: pictureLink,
                        description: wolfDescription
                    }
                }

                let fetchConfig = {
                    method: "POST",
                    headers: {"Content-Type": "application/json"},
                    body: JSON.stringify(fetchBody)
                }

                fetch(url+complement, fetchConfig)
                .then(resp => console.log(resp.json))
                .catch(error => console.log(error))
        
            }
            else {alert("Nome ou descrição são pequenos ou grandes demais!")}
        }
        else{alert("Digite um valor inteiro entre 0 e 100.")}
    }
    else{alert("Preencha todos os campos!")}

})
