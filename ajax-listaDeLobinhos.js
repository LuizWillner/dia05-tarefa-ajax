const url = "http://lobinhos.herokuapp.com/";
const complement = "wolves/";
const complement2 = "adopted/";

const main = document.querySelector("main");
const checkbox = document.querySelector("#filter");


const addWolfButton = document.querySelector(".add-wolf-button");

function goToAdicionarLobinho() {
    window.open("adicionarLobinho.html");
}

function fillPage(wolfName, wolfAge, wolfPic, wolfDescription, wolfID, reverse, adoptionStatus) {
    let new_div = document.createElement("div");
    new_div.classList.add(`lobo${reverse}`);
    new_div.id = `divLobinho-${wolfID}`;
    new_div.innerHTML = `
    <a class="lobo-picture-${reverse}"  href="mostrarLobinho.html?id=${wolfID}"><img src="${wolfPic}"></a>

    <div class="cardContent">
        <div class="cardName">
            <div class="name-button-${reverse}">
                <h3>${wolfName}</h3>
                <button class="botaoAdotar" id="${adoptionStatus}">${adoptionStatus}</button>
            </div>
            <p>Idade: ${wolfAge} anos</p>
        </div>

        <div class="cardDescription${reverse}">
            <p>${wolfDescription}</p>
        </div>

    </div>`;

    main.appendChild(new_div);
}

// <div class="lobo0">
    // <div class="lobo-picture-0"><img src="AdoteUmLobinho/image_1.png"></div>

    // <div class="cardContent">
    //     <div class="cardName">
    //         <div class="name-button-0">
    //             <h3>Nome do Lobo</h3>
    //             <button class="botaoAdotar" href="mostrarLobinho.html?id=1">Adotar</button>
    //         </div>
    //         <p>Idade: XX anos</p>
    //     </div>

    //     <div class="cardDescription0">
    //         <p>Não obstante, o surgimento do comércio virtual faz parte de um processo de gerenciamento
    //             do levantamento das variáveis envolvidas. Não obstante, o surgimento do comércio virtual
    //             faz parte de um processo de gerenciamento do levantamento das variáveis envolvidas.
    //             Não obstante, o surgimento do comércio virtual faz parte de um processo de gerenciamento
    //             do levantamento das variáveis envolvidas. Não obstante, o surgimento do comércio virtual
    //             faz parte de um processo de gerenciamento do levantamento das variáveis envolvidas.</p>
    //     </div>

    // </div>

// </div>

// ================ GET (não adotados) =================================
fetch(url+complement)
.then(resp => { return resp.json()})
.then(allWolves => {
    allWolves["wolves"].forEach((wolf, index) => {
        fillPage(wolf.name, wolf.age, wolf.link_image, wolf.description, wolf.id, index%2, "Adotar");
    });
})
.catch(error => console.log(error));



checkbox.addEventListener("click", event => {
     // ================ GET (adotados) =================================
     console.log(event.target.checked);
    if (event.target.checked) {
        fetch(url+complement+complement2)
        .then(resp => { return resp.json()})
        .then(allWolves => {
            allWolves["wolves"].forEach((wolf, index) => {
                fillPage(wolf.name, wolf.age, wolf.link_image, wolf.description, wolf.id, index%2, "Adotado");
            });
        })
        .catch(error => console.log(error));
    }

    else {
        // ================ GET (não adotados) =================================
        fetch(url+complement)
        .then(resp => { return resp.json()})
        .then(allWolves => {
            allWolves["wolves"].forEach((wolf, index) => {
                fillPage(wolf.name, wolf.age, wolf.link_image, wolf.description, wolf.id, index%2, "Adotar");
            });
        })
        .catch(error => console.log(error));
    }
})

